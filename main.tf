terraform {
  backend "s3" {
    bucket         = "mytfstatebucketpubudu"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "tfstatedbpubudu"
  }
}

module "vpc" {
    source = "./vpc"  
}

module "ec2" {
  source = "./web"
  sn = module.vpc.pb_sn
  sg = module.vpc.sg
}


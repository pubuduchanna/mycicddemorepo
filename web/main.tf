resource "aws_instance" "server" {
  ami = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  subnet_id = var.sn
  security_groups = [ var.sg ]
  tags = {
    name = "myserver"
  }
}